@extends('layouts.app')

@section('title', 'Tienda Online')

@section('content')
        @foreach ($data as $producto)
         <a href="producto/{{ $producto->cod }}" class="rk-item ae-masonry__item">
                <img src="./img/{{ $producto->url }}.jpg" alt="">
                <div class="item-meta">
                  <h2>{{ $producto->titulo }}</h2>
                  <p>{{ $producto->categoria }}</p>
                </div>
            </a>

        @endforeach
           

@endsection



