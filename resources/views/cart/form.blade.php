<!DOCTYPE html>
<html>
<head>
	<title>Tienda Virtual</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<div class="container mt-5">
		<form action="{{ url('add') }}" method="post">
		<input type="hidden" name="_token" value="{{ csrf_token() }} ">
			<section>
				<div class="form-group" >
    				<label for="id" class="font-weight-bold">Codigo Identificador del Producto Id</label>
				    <input name="id" type="text" required="true" class="form-control" id="id" aria-describedby="id" placeholder="id">
				    <small id="emailHelp" class="form-text text-muted">Id de Producto.</small>
				</div>
				<div class="form-grop">
					<label for="name" class="font-weight-bold">Nombre del Producto</label>
					<input class="form-control" type="text" name="name" required="true" value="" placeholder="Nombre del Producto">
				</div>

				<div class="form-grop">
					<label for="price" class="font-weight-bold">Precio del Producto</label>
					<input class="form-control" type="text" name="price" required="true" placeholder="Precio del Producto">
				</div>

				<div class="form-grop">
					<label for="quantity" class="font-weight-bold">Cantidad en existencia del Producto</label>
					<input class="form-control" type="number" required="true" name="quantity" placeholder="Cantidad en existencia del Producto">
				</div>

				<div class="form-grop">
					<label for="quantity" class="font-weight-bold">Color</label>
					<select class="form-control" name="attributes[color]">
						<option value="red">Rojo</option>
						<option value="blanco">Blanco</option>
						<option value="azul">Azul</option>
					</select>
				</div>

				<div class="mt-2"> 
					<button type="submit" class="btn btn-primary"> Registar </button>
				</div>
			</section>	
		</form>
	</div>
	
</body>
</html>