@extends('layouts.compra')

@section('title', 'Tienda Online')

@section('content')
		<div class="row">
			<div class="col-lg-12">
				<h4 class="text-right"> Total a Pagar {{ $total  }}$</h4>
			</div>
    			
    	</div>
    	<div class="card-deck mt-3 col-md-12 col-lg-12">
    			<div class="pt-2 mb-5">
    				<?php $cont = 0; ?>
    				@foreach ($data as $producto)
							<div class="pt-4 mb-5">
				  				<div class="card pt-2">
				  					<img class="card-img-top img-responsive p-4" src="{{URL::to('/')}}/productos/{{ $producto->id }}/main.jpg" alt=""/>
									  <div class="card-body">
									    <h5 class="card-title">{{ $producto->name }}</h5>
									    <p class="card-text">
									    	<p>Codigo del Producto: {{ $producto->id }}</p>
									    	<p>Cantidad: {{ $producto->quantity }}</p>
									    	<p>Precio Unitario:  {{ $producto->price }}$</p>
									    	<p class="">SubTotal:   {{ $producto->price * $producto->quantity}}$</p>
									    </p>
									    <hr>
									    <a href="delete/{{ $producto->id }} " class="btn btn-primary">Eliminar</a>
									  </div>
								</div>
						</div>
				@endforeach
    		
    	</div>

    	
@endsection