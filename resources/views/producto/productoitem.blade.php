@extends('layouts.compra')

@section('title', 'Tienda Online')

@section('content') 
        <div class="ae-grid ae-grid--collapse au-xs-ptp-1 au-xs-pbp-1">
        	
	        <div class="ae-grid__item item-lg-8 item-lg--offset-2">
	          	<img src="{{ URL::to('/') }}/productos/{{ $id }}/pic-1.jpg" alt="Producto">
	            <h4 class="ae-u-bolder" style="padding-top: 25% ">{{$producto['producto']}}</h4>
	            <p class="ae-eta">{{$producto['subtitulo1']}}</p>
	            <p class="ae-eta">{{$producto['subtitulo2']}}</p>

	            <form action="../add" method="post" accept-charset="utf-8">
		            <input type="hidden" name="_token" value="{{ csrf_token() }} ">	
		        	<input type="hidden" name="">
		            <input name="id" type="hidden" value="{{$producto['id']}}" id="id">
					<input type="hidden" name="name"  value="{{$producto['name']}}">
					<input type="hidden" name="price"  value="{{$producto['price']}}">
					<p><label> <h3>Precio del Producto: {{ $producto['price'] }}$ </h3></label></p>
					<p><input class="rk-search-field" type="number" required="true" name="quantity" placeholder="Cantidad" ></p>
					<p><button type="submit" class="arrow-button" style="">Añadir al carrito</button></p>
	        	</form>	
	        </div>
        </div>
       
@endsection
