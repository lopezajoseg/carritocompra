<html>
    <head>
        <title>Caracas - @yield('title')</title>
        <link rel="stylesheet" href="{{URL::to('/')}}/css/aurora-pack.min.css">
        <link rel="stylesheet" href="{{URL::to('/')}}/css/aurora-theme-base.min.css">
        <link rel="stylesheet" href="{{URL::to('/')}}/css/urku.css">
        <link rel="stylesheet" href="{{URL::to('/')}}/css/aap.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
         <!-- development version, includes helpful console warnings -->
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    </head>
    <body class="top-fixed" id="app">
    <header class="ae-container-fluid ae-container-fluid--full rk-header ">
      <input type="checkbox" id="mobile-menu" class="rk-mobile-menu">
      <div class="ae-container-fluid rk-topbar">
        <h1 class="rk-logo"><a href="/">Tienda Virtual<sup>tm</sup></a></h1>
        <nav class="rk-navigation">
          <ul class="rk-menu">
            <li class="rk-menu__item"><a href="/" class="rk-menu__link">Home</a>
            </li>
            <li class="active rk-menu__item"><a href="cart" class="rk-menu__link">Carrito</a>
            </li>
          </ul>
          <form class="rk-search">
            <input type="text" placeholder="Search" id="urku-search" class="rk-search-field">
            <label for="urku-search">
              <svg>
                <use xlink:href="./img/symbols.svg#icon-search"></use>
              </svg>
            </label>
          </form>
        </nav>
      </div>
    </header>
    <section class="ae-container-fluid rk-main">
          <section class="ae-container-fluid ae-container-fluid--inner rk-portfolio">
          
                 @yield('content')   
           
          </section>
    </section>
    @section('footer')
  <footer id="sticky-footer" class="fixed-bottom py-4 bg-dark text-white-50">
    <div class="container text-center">
      <small>Copyright &copy; Developer José Gregorio López Arias Email: lopezajoseg@gmail.com</small>
    </div>
  </footer>
     @show

    </body>
    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</html>