<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;

class cartController extends Controller
{

	function home(){
		$client = new \GuzzleHttp\Client(); //GuzzleHttp\Client	
		$res = $client->request('GET', 'https://portafolio-d8e89.firebaseio.com/productos_idx.json');
		$data = json_decode($res->getBody());
		//return $cargar;
		return view('welcome',[ 'data' => $data ]);
	}

	function producto($id){

		$client = new \GuzzleHttp\Client(); //GuzzleHttp\Client	
		$servicio = 'https://portafolio-d8e89.firebaseio.com/productos/'.$id.'.json';
		$res = $client->request('GET', $servicio);
		//$res->getBody();
		$data = json_decode($res->getBody(), JSON_FORCE_OBJECT);
		
		return view('producto.productoitem',[ 'producto' => $data, 'id'=> $id ]);
	}

    public function cart(){
    	$data  = Cart::getContent();
    	$total = Cart::getTotal();
    	$subTotal = Cart::getSubTotal();
    	return view('cart.cart',[ 'data' => $data, 'total'=>$total, 'subTotal' =>$subTotal ]);
    }

    public function add (Request $resp){
    	//return $resp;
    	//return $resp['attributes']['color'];
    	try {
    		$add = Cart::add(
    		[
    		'id' => $resp->id,
    		'price' => $resp->price,
    		'quantity'=> $resp->quantity,
    		'name'=> $resp->name,
	    		'attributes' => array(
	    		[
	    			'size'   => '15',
	    			'img'    =>''
	    		])
    		]);
    	} catch (Exception $e) {
    		return "Error al carga producto al carrito".$e;
    	}
    	
    	if($add){
    		return redirect('cart');
    	}
    }

    function clear(){

    	Cart::clear();
    	return "Borrar Carrito";
    }

    function eliminarCart($id){

    	Cart::remove($id);
    	return redirect('cart');

    }
}
